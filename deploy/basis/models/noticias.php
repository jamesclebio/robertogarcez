<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Models;

class Noticias
{
  public function index() {
    $feed = file_get_contents('http://www.conjur.com.br/rss.xml');
    $rss = new \SimpleXmlElement($feed);

    return $rss->channel->item;
  }
}
