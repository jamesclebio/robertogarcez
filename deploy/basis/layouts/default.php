<!DOCTYPE html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?>" lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->getTitle(); ?></title>
  <meta name="description" content="<?php echo $this->getDescription(); ?>">
  <link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
  <?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
  <?php $this->getAnalytics(); ?>
  <?php $this->getBodyPrepend(); ?>

  <header class="default-header">
    <div class="default-header-container">
      <h1 class="logo"><a href="<?php echo $this->_url('root'); ?>#!/inicio">Roberto Garcez</a></h1>

      <div class="links">
        <ul>
          <li><a href="#!/correspondente"><span class="icon-briefcase"></span> Correspondente jurídico</a></li>
          <li><a href="#!/escritorios"><span class="icon-location"></span> Escritório</a></li>
          <li><a href="tel:07999999999"><span class="icon-android-call"></span> (79) 9999-9999</a></li>
        </ul>
      </div>

      <nav class="default-nav">
        <ul>
          <li><a href="<?php echo $this->_url('root'); ?>#!/inicio">Início</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/sobre">Sobre</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/atuacao">Áreas de atuação</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/noticias">Notícias</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/publicacoes">Publicações</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/eventos">Eventos</a></li>
          <li><a href="<?php echo $this->_url('root'); ?>#!/contato">Contato</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <?php if (@$this->billboard): ?>
  <div class="billboard">
    <div class="container">
      <div class="item item-template-right" style="background-image: url(<?php echo $this->_asset('default/images/billboard/hammer.jpg'); ?>);">
        <div class="item-container">
          <div class="pane background-color-transparent">
            <div class="heading">
              <h2 class="color-white">Lorem ipsum dolor<br> sit amet facere</h2>
            </div>
            <div class="text">
              <p class="color-white">Incidunt officia error obcaecati esse<br> consequatur facere, dolorem quaerat minima vitae</p>
            </div>
          </div>
        </div>
      </div>

      <div class="item item-template-left" style="background-image: url(<?php echo $this->_asset('default/images/billboard/hammer-2.jpg'); ?>);">
        <div class="item-container">
          <div class="pane background-color-transparent">
            <div class="heading">
              <h2 class="color-white">Persp modi atque<br> dolor sit amet</h2>
            </div>
            <div class="text">
              <p class="color-white">Reiciendis totam consectetur,<br> natus similique molestiae dolore quidem cupiditate</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="default-content">
    <?php $this->getView(); ?>

    <section class="section-content" id="escritorios">
      <div class="section-content-container">
          <div class="supericon">
            <span class="icon-location"></span>
          </div>

          <ul>
            <li>
              <address class="address address-main">
                <div class="address-heading">Aracaju</div>
                <div class="address-item location">
                  Rua Arauá, 383<br>
                  Bairro Centro<br>
                  CEP 49010-330<br>
                  <strong>Aracaju-SE</strong>
                </div>
                <div class="address-item phone"><a href="tel:07999999999">(79) 9999-9999</a></div>
                <div class="address-item email"><a href="mailto:contato@robertogarcez.adv.br">contato@robertogarcez.adv.br</a></div>
              </address>
            </li>
          </ul>
      </div>
    </section>

    <section class="section-content" id="correspondente">
      <div class="section-content-container">
        <header>
          <h1>Correspondente jurídico</h1>
          <h3>Use o formulário para enviar uma mensagem:</h3>
        </header>

        <form id="form-correspondent" method="post" action="" class="form">
          <input type="text" name="spamtrap" placeholder="Not fill this field" class="display-none">
          <fieldset>
            <legend>Contato</legend>
            <div class="grid grid-items-2">
              <div class="grid-item">
                <label>Nome *<input name="name" type="text" required></label>
                <label>CPF *<input name="cpf" type="text" class="mask-cpf" required></label>
                <label>Email *<input name="email" type="email" required></label>
                <label>Telefone *<input name="phone" type="text" class="mask-phone" required></label>
              </div>
              <div class="grid-item">
                <label>Mensagem *<textarea name="message" cols="30" rows="10" maxlength="600" class="height-275" data-field-count required></textarea></label>
                <label class="check"><input name="mailing" type="checkbox" value="1" checked>Desejo receber novidades por email</label>
              </div>
            </div>

            <div class="pane-action">
              <button type="submit" class="button button-large button-bordered button-custom-2">Enviar</button>
            </div>
          </fieldset>
        </form>
      </div>
    </section>
  </div>

  <footer class="default-footer">
    <div class="copyright">
      <div class="main-container">
        <ul class="links">
          <li><a href="<?php echo $this->_url('termos'); ?>">Termos de uso</a></li>
          <li><a href="<?php echo $this->_url('privacidade'); ?>">Política de privacidade</a></li>
        </ul>
        <p>©2015 <strong>Roberto Garcez</strong>. Todos os direitos reservados.</p>
      </div>
    </div>
  </footer>

  <a href="#" id="scroll-top" title="Ir para o início"></a>

  <script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
  <?php $this->getBodyAppend(); ?>
</body>
</html>
