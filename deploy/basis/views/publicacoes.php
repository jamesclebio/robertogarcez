<section class="section-content">
	<div class="section-content-container">
    <div class="breadcrumb">
      <ul>
        <li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
        <li>Publicações</li>
      </ul>
    </div>

		<header>
			<h1>Publicações</h1>
		</header>

    <div class="separate-top margin-bottom-30">
      <form action="" method="get" class="form form-inline">
        <fieldset>
          <legend>Filtro</legend>
          <div class="grid grid-items-5">
            <div class="grid-item">
              <select name="t">
                <option value="0" selected>Tudo</option>
                <option value="1">Artigos</option>
                <option value="2">Jurisprudências</option>
              </select>
            </div>
            <div class="grid-item">
              <input name="d" type="text" placeholder="Data" data-field-picker="date">
            </div>
            <div class="grid-item grid-item-span-2">
              <input name="q" type="text" placeholder="O que você procura?">
            </div>
            <div class="grid-item">
              <button type="submit" class="button button-block button-secondary"><span class="icon-search"></span> Pesquisar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>

    <div class="list-links">
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
      <li>
        <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
          <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          <div class="resume">Laborum reiciendis placeat molestias reprehenderit sint, beatae deserunt qui tempora eveniet debitis, id deleniti, laboriosam adipisci fuga quasi impedit nam est magnam libero illum voluptate vitae aperiam esse fugiat inventore quod vel mollitia dolorem...</div>
        </a>
      </li>
    </div>

    <div class="pagination">
      <ul>
        <li><a href="#"><span class="icon-arrow-left-b"></span></a></li>
        <li class="current"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><span>...</span></li>
        <li><a href="#">6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">8</a></li>
        <li><a href="#">9</a></li>
        <li><a href="#">10</a></li>
        <li><a href="#"><span class="icon-arrow-right-b"></span></a></li>
      </ul>
    </div>
	</div>
</section>
