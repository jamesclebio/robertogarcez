<section class="section-content">
	<div class="section-content-container">
    <div class="breadcrumb">
      <ul>
        <li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
        <li>Notícias</li>
      </ul>
    </div>

		<header>
			<h1>Notícias</h1>
		</header>

    <div class="separate-top margin-bottom-30">
      <form action="http://www.conjur.com.br/busca" method="get" target="_blank" class="form form-inline">
        <fieldset>
          <legend>Filtro</legend>
          <div class="grid grid-items-5">
            <div class="grid-item grid-item-span-4">
              <input name="busca" type="text" placeholder="O que você procura?">
            </div>
            <div class="grid-item">
              <button type="submit" class="button button-block button-secondary"><span class="icon-search"></span> Pesquisar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>

    <div class="list-links">
      <?php foreach ($this->noticias() as $item): ?>
        <li>
          <a href="<?php echo $item->guid; ?>" target="_blank">
            <div class="title"><?php echo $item->title; ?></div>
            <div class="resume"><?php echo $item->description; ?></div>
          </a>
        </li>
      <?php endforeach; ?>
    </div>
	</div>
</section>
