<section class="section-content">
  <div class="section-content-container">
    <div class="breadcrumb">
      <ul>
        <li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
        <li>Termos de uso</li>
      </ul>
    </div>

    <header>
      <h1>Termos de uso</h1>
    </header>

    <div class="text">
      <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, expedita, nesciunt sequi suscipit reprehenderit eius. At, ratione dolor esse doloribus et illo mollitia provident quaerat ex consequuntur eum labore in!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni provident, rerum expedita. Beatae possimus numquam consequatur eligendi id maiores officia iste eius, eum aliquam nam magnam dolorum, eveniet, quia asperiores dolorem fuga blanditiis ex quibusdam modi. At numquam vero voluptas alias doloribus, magnam voluptatibus tempora voluptatum, quod soluta ratione architecto.</p>

      <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quae sequi sit voluptas odio, voluptatum ipsa aliquid eius doloribus, dolorum eos beatae adipisci totam maiores alias quos placeat quas et!</p>
      <ul>
        <li>Lorem ipsum dolor sit amet.</li>
        <li>Lorem ipsum dolor sit amet.</li>
        <li>Lorem ipsum dolor sit amet.</li>
      </ul>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus at voluptatem perspiciatis veritatis accusamus illo! Laboriosam, nesciunt animi incidunt corrupti harum optio vero explicabo rerum similique rem voluptates blanditiis.</p>
    </div>
  </div>
</section>
