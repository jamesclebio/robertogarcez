<section class="section-quote">
  <blockquote>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, praesentium? Doloremque dicta a magni, nostrum velit repellat, ipsa deleniti expedita. Culpa eveniet earum aut quam pariatur debitis repudiandae dolores consequuntur, reprehenderit quaerat vero consequatur tempore officiis voluptatem.</p>
    <cite>Roberto Garcez</cite>
  </blockquote>
</section>

<section class="section-content" id="sobre">
  <div class="section-content-container">
    <header>
      <h1>Roberto Garcez</h1>
      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing</h3>
    </header>

    <div class="align-center">
      <div class="block-avatar">
        <img src="<?php echo $this->_asset('default/images/robertogarcez.jpg'); ?>" alt="">
      </div>
    </div>

    <div class="margin-top-25 text">
      <p>Labore perspiciatis hic pariatur iste ipsa molestias quia tempora ipsum similique inventore doloribus praesentium odio quo voluptate, soluta repellat ea culpa at, accusamus esse atque nulla laborum, corporis.</p>
      <p>At ducimus eum exercitationem asperiores harum esse delectus explicabo cum. Officiis laboriosam pariatur, cum voluptatibus quidem assumenda, enim officia optio, labore doloribus nulla. Maiores molestiae totam obcaecati aliquam tempore itaque, odio assumenda quisquam necessitatibus, nulla repellat velit! Magnam sapiente libero soluta similique sint vitae quisquam dicta deserunt porro natus dolores enim officiis quia, quaerat impedit laboriosam ex! Veniam iusto facere dolores repellat sit molestias impedit dolor pariatur laborum, velit libero voluptatum voluptates.</p>
    </div>
  </div>
</section>

<section class="section-content" id="atuacao">
  <div class="section-content-container">
    <header>
      <h1>Áreas de atuação</h1>
      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing</h3>
    </header>

    <div class="grid grid-items-4">
      <div class="grid-item">
        <div class="pane-icon-rounded">
          <div class="image"><span class="icon-briefcase"></span></div>
          <div class="title">Direito do trabalho</div>
          <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis placeat, voluptas. Dignissimos voluptatum architecto doloribus.</div>
        </div>
      </div>
      <div class="grid-item">
        <div class="pane-icon-rounded">
          <div class="image"><span class="icon-card"></span></div>
          <div class="title">Direito do Consumidor</div>
          <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam accusamus fugiat dolorum eaque eum dignissimos?</div>
        </div>
      </div>
      <div class="grid-item">
        <div class="pane-icon-rounded">
          <div class="image"><span class="icon-person"></span></div>
          <div class="title">Direito Civil</div>
          <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus fugit, ratione nulla error inventore laudantium?</div>
        </div>
      </div>
      <div class="grid-item">
        <div class="pane-icon-rounded">
          <div class="image"><span class="icon-ios-people"></span></div>
          <div class="title">Direito Previdenciário</div>
          <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo aliquid, aperiam eos quia dolorum repellendus!</div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-content" id="noticias">
  <div class="section-content-container">
    <header>
      <h1>Notícias</h1>
      <h3>Informações sobre Direito e Justiça</h3>
    </header>

    <div class="grid grid-items-4">
      <?php foreach ($this->noticias(4) as $item): ?>
        <div class="grid-item">
          <a href="<?php echo $item->guid; ?>" target="_blank" class="block-post">
            <div class="image">
              <img src="<?php echo $this->_asset('default/images/post-sample-thumb.jpg'); ?>" alt="">
            </div>
            <div class="title"><?php echo $item->title; ?></div>
            <div class="resume"><?php echo substr($item->description, 0, 110); ?>...</div>
          </a>
        </div>
      <?php endforeach; ?>
    </div>

    <div class="margin-top-40 align-center">
      <a href="<?php echo $this->_url('noticias'); ?>" class="button button-bordered button-custom-1"><span class="icon-plus-circled"></span> Ver todas as notícias</a>
    </div>
  </div>
</section>

<section class="section-content" id="publicacoes">
  <div class="section-content-container">
    <header>
      <h1>Publicações</h1>
      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
    </header>

    <div class="grid grid-items-2">
      <div class="grid-item">
        <h4 class="heading separate-bottom align-center">Artigos</h4>
        <div class="list-links">
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
        </div>
      </div>

      <div class="grid-item">
        <h4 class="heading separate-bottom align-center">Jurisprudências</h4>
        <div class="list-links">
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
          <li>
            <a href="<?php echo $this->_url('publicacoes/conteudo'); ?>">
              <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
            </a>
          </li>
        </div>
      </div>
    </div>

    <div class="margin-top-40 align-center">
      <a href="<?php echo $this->_url('publicacoes'); ?>" class="button button-bordered button-custom-2"><span class="icon-plus-circled"></span> Ver todas as publicações</a>
    </div>
  </div>
</section>

<section class="section-content" id="eventos">
  <div class="section-content-container">
    <header>
      <h1>Eventos</h1>
    </header>

    <div class="main-gallery">
      <div class="main-gallery-index">
        <h5>Selecione o evento</h5>
        <ul>
          <li class="on"><a href="<?php echo $this->_url('gallery/view/album/sample-1'); ?>">Título do evento 1</a></li>
          <li><a href="<?php echo $this->_url('gallery/view/album/sample-2'); ?>">Título do evento 2</a></li>
          <li><a href="<?php echo $this->_url('gallery/view/album/sample-3'); ?>">Título do evento 3</a></li>
        </ul>
      </div>

      <div class="main-gallery-thumbs"></div>
    </div>
  </div>
</section>

<section class="section-content" id="contato">
  <div class="section-content-container">
    <header>
      <h1>Dúvidas e informações processuais</h1>
      <h3>Use o formulário para enviar uma mensagem:</h3>
    </header>

    <form id="form-contact" method="post" action="" class="form">
      <input type="text" name="spamtrap" placeholder="Not fill this field" class="display-none">
      <fieldset>
        <legend>Contato</legend>
        <div class="grid grid-items-2">
          <div class="grid-item">
            <label>Nome *<input name="name" type="text" required></label>
            <label>CPF *<input name="cpf" type="text" class="mask-cpf" required></label>
            <label>Email *<input name="email" type="email" required></label>
            <label>Telefone *<input name="phone" type="text" class="mask-phone" required></label>
          </div>
          <div class="grid-item">
            <label>Mensagem *<textarea name="message" cols="30" rows="10" maxlength="600" class="height-275" data-field-count required></textarea></label>
            <label class="check"><input name="mailing" type="checkbox" value="1" checked>Desejo receber novidades por email</label>
          </div>
        </div>

        <div class="pane-action">
          <button type="submit" class="button button-large button-bordered button-custom-2">Enviar</button>
        </div>
      </fieldset>
    </form>
  </div>
</section>
