<section class="section-content">
	<div class="section-content-container">
    <div class="breadcrumb">
      <ul>
        <li><a href="<?php echo $this->_url('root'); ?>">Início</a></li>
        <li><a href="<?php echo $this->_url('noticias'); ?>">Notícias</a></li>
        <li>Conteúdo</li>
      </ul>
    </div>

    <div class="text">
      <h1>Aperiam quam aliquam porro laborum cupiditate praesentium</h1>

      <div class="separate-top margin-top-30 align-right">
        <?php include 'share.php'; ?>
      </div>

      <div class="text margin-top-30">
        <div class="block-media block-media-left">
          <a href="<?php echo $this->_asset('default/images/post-sample-large.jpg'); ?>" class="lightbox"><img src="<?php echo $this->_asset('default/images/post-sample-thumb.jpg'); ?>" alt=""></a>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur repellendus explicabo maxime obcaecati magni tempore, facilis nemo recusandae ratione nam quod voluptates reiciendis voluptate dolor officia esse quia. Dolorem, ratione unde harum molestiae iste quasi error accusantium voluptates rerum dicta.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas architecto corporis quibusdam, eaque sapiente, similique ut possimus iusto voluptate voluptatem rem amet consequuntur nihil necessitatibus dignissimos, eum assumenda neque ad libero maxime, laborum quae veniam explicabo qui! Eveniet dolore quasi dignissimos. Ducimus placeat aliquam minus, velit saepe, illum cumque culpa quaerat iusto perspiciatis, esse necessitatibus, at? Ipsa tempore reprehenderit, adipisci facere ducimus dicta explicabo ex inventore dolores pariatur aliquam ipsam consequuntur cupiditate id, consectetur voluptatem blanditiis autem nobis beatae modi, est sint a ab obcaecati nemo! Necessitatibus repudiandae corporis labore, saepe dignissimos voluptatem, numquam in nam. Asperiores modi commodi, provident facere veniam officia et, recusandae rerum eius magnam necessitatibus totam incidunt tempore minima maiores perspiciatis quia sit sint nemo fuga.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt libero sapiente obcaecati non cumque temporibus, impedit quod quo laborum officia molestias nihil eaque laboriosam magnam ad quos! Corporis maiores incidunt id ducimus, repudiandae tenetur necessitatibus omnis amet voluptatum odit quasi rerum sunt iusto dolores placeat perspiciatis consectetur porro est. Ea magni numquam magnam neque saepe deleniti reiciendis aperiam sed ipsa, nostrum, quasi expedita, vitae sunt.</p>
      </div>
    </div>

    <div class="separate-top margin-top-30">
      <h5 class="heading">Veja também</h5>
      <div class="list-links">
        <li>
          <a href="<?php echo $this->_url('noticias/conteudo'); ?>">
            <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->_url('noticias/conteudo'); ?>">
            <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->_url('noticias/conteudo'); ?>">
            <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->_url('noticias/conteudo'); ?>">
            <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->_url('noticias/conteudo'); ?>">
            <div class="title">Possimus voluptas minima eveniet quibusdam placeat voluptates quos quo sapiente</div>
          </a>
        </li>
      </div>
      <div class="margin-top-30 align-right">
        <a href="<?php echo $this->_url('noticias'); ?>" class="button button-bordered button-secondary"><span class="icon-plus-circled"></span> Ver todas as notícias</a>
      </div>
    </div>
	</div>
</section>
