<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Contato extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
    $this->setTitle('');
    $this->setDescription('');
    $this->setAnalytics(true);
    // $this->mailchimp();
    $this->mail();
  }

  public function mailchimp() {
    if ($_POST && !$_POST['spamtrap'] && $_POST['mailing']) {
      require 'basis/helpers/mailchimp.php';

      // Dev account
      define('MAILCHIMP_API_KEY',  '6b7dbce2bb3b5b73d63c1abc00ae8979-us4');
      define('MAILCHIMP_LIST_ID',  '546dc774c9');

      $mailchimp = new \MCAPI(MAILCHIMP_API_KEY);
      $fields = array(
        'MAIL_NAME' => $_POST['name'],
        'MAIL_PHONE' => $_POST['phone']
      );

      $mailchimp->listSubscribe(MAILCHIMP_LIST_ID, $_POST["email"], $fields, 'html', false, true);
    }
  }

  public function mail() {
    if ($_POST && !$_POST['spamtrap']) {
      $sender = 'dev@jamesclebio.com.br';
      $mail = new \Basis\Helpers\Mail();

      $mail->setHeader(
          'From: ' . $_POST['name'] . ' <' . $sender . '>' . "\r\n" .
          'Return-Path: ' . $sender . "\r\n" .
          'Reply-To: ' . $_POST['email'] . "\r\n"
      );

      $mail->setTo($sender);
      $mail->setSubject('Contato');
      $mail->setBody(
          'Nome: ' . $_POST['name'] . "\n" .
          'CPF: ' . $_POST['cpf'] . "\n" .
          'E-mail: ' . $_POST['email'] . "\n" .
          'Telefone: ' . $_POST['phone'] . "\n\n" .
          '---' . "\n\n" .
          'Mensagem: ' . $_POST['message'] . "\n\n" .
          '---' . "\n\n" .
          'Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
      );

      $mail->setAlertSuccess('<p><strong>Sua mensagem foi enviada com sucesso!</strong></p><p>Daremos um retorno o mais breve possível.</p>');
      $mail->send($this, $this->_url('contato'), $this->_url('contato'), '-r' . $sender);
    }
  }
}
