<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Privacidade extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('privacidade');
    $this->setTitle('Roberto Garcez - Política de privacidade');
    $this->setDescription('');
    $this->setAnalytics(true);
  }
}
