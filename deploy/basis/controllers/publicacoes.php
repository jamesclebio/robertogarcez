<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Publicacoes extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('publicacoes');
    $this->setTitle('Roberto Garcez - Publicações');
    $this->setDescription('');
    $this->setAnalytics(true);
  }

  public function conteudo() {
    $this->setView('publicacoes-conteudo');
  }
}
