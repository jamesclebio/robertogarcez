<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Termos extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('termos');
    $this->setTitle('Roberto Garcez - Termos de uso');
    $this->setDescription('');
    $this->setAnalytics(true);
  }
}
