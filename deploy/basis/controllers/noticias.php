<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Noticias extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('noticias');
    $this->setTitle('Roberto Garcez - Notícias');
    $this->setDescription('');
    $this->setAnalytics(true);
  }

  public function noticias() {
    $noticias = new \Basis\Models\Noticias;

    return $noticias->index();
  }

  public function conteudo() {
    $this->setView('noticias-conteudo');
  }
}
