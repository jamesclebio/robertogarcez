<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Index extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('index');
    $this->setTitle('Roberto Garcez');
    $this->setDescription('');
    $this->setAnalytics(true);
    $this->setHtmlClass('layout-billboard');
    $this->billboard = true;
  }

  public function noticias($show = 2) {
    $noticias = new \Basis\Models\Noticias;
    $return = array();

    foreach ($noticias->index() as $item) {
      if ($show > 0) {
        array_push($return, $item);
        $show--;
      } else {
        break;
      }
    }

    return $return;
  }
}
