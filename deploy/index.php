<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Core;

// Timezone
const TIMEZONE = 'America/Sao_Paulo';

// Route controller
$routeController = array(
  'home' => 'index'
);

// Application bootstrap
require_once 'basis/core/bootstrap.php';
