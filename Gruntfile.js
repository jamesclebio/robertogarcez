module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    project: {
      banner: '/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
      default: {
        scripts: {
          base: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/jquery-maskedinput/dist/jquery.maskedinput.js',
            'bower_components/datetimepicker/jquery.datetimepicker.js',
            'bower_components/carouFredSel/jquery.carouFredSel-6.2.1.js',
            'bower_components/colorbox/jquery.colorbox-min.js',
            'bower_components/colorbox/i18n/jquery.colorbox-pt-br.js',
            'bower_components/imagesloaded/imagesloaded.pkgd.min.js'
          ],

          main: [
            '<%= project.default.scripts.base %>',
            'assets/default/scripts/main.core.js',
            'assets/default/scripts/main.share.js',
            'assets/default/scripts/main.form.js',
            'assets/default/scripts/main.alert.js',
            'assets/default/scripts/main.collapse.js',
            'assets/default/scripts/main.togglebox.js',
            'assets/default/scripts/main.scrollto.js',
            'assets/default/scripts/main.modal.js',
            'assets/default/scripts/main.dropdown.js',
            'assets/default/scripts/main.scroll.js',
            'assets/default/scripts/main.nav.js',
            'assets/default/scripts/main.field-extend.js',
            'assets/default/scripts/main.field-count.js',
            'assets/default/scripts/main.field-select.js',
            'assets/default/scripts/main.field-picker.js',
            'assets/default/scripts/main.billboard.js',
            'assets/default/scripts/main.lightbox.js',
            'assets/default/scripts/main.gallery.js',
            'assets/default/scripts/main.init.js'
          ]
        },

        styles: {
          path: 'assets/default/styles',
          compile: '<%= project.default.styles.path %>/**/*.sass'
        }
      }
    },

    jshint: {
      options: {
        force: true
      },

      default_main: ['assets/default/scripts/**/*']
    },

    concat: {
      options: {
        banner: '<%= project.banner %>\n'
      },

      default: {
        src: '<%= project.default.scripts.main %>',
        dest: 'deploy/assets/default/scripts/main-source.js'
      }
    },

    uglify: {
      options: {
        banner: '<%= project.banner %>\n'
      },

      default_main: {
        src: '<%= project.default.scripts.main %>',
        dest: 'deploy/assets/default/scripts/main.js'
      }
    },

    compass: {
      options: {
        banner: '<%= project.banner %>',
        relativeAssets: true,
        noLineComments: true,
        outputStyle: 'compressed',
        raw: 'preferred_syntax = :sass'
      },

      default: {
        options: {
          sassDir: '<%= project.default.styles.path %>',
          specify: '<%= project.default.styles.compile %>',
          cssDir: 'deploy/assets/default/styles',
          imagesDir: 'deploy/assets/default/images',
          javascriptsDir: 'deploy/assets/default/scripts',
          fontsDir: 'deploy/assets/default/fonts'
        }
      }
    },

    watch: {
      options: {
        livereload: true,
        spawn: false
      },

      grunt: {
        files: ['Gruntfile.js'],
        tasks: ['default']
      },

      default_scripts_main: {
        files: ['<%= project.default.scripts.main %>'],
        tasks: ['jshint:default_main', 'uglify:default_main']
      },

      default_styles: {
        files: ['<%= project.default.styles.compile %>'],
        tasks: ['compass:default']
      },

      app: {
        files: ['deploy/**/*']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint', 'uglify', 'compass']);
};
