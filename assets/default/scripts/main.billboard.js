;(function($, window, document, undefined) {
  'use strict';

  main.billboard = {
    wrapper: '.billboard',

    build: function() {
      var that = this,
        $wrapper = $(that.wrapper),
        $container = $wrapper.find('.container'),
        $itemFirst = $container.find('.item:first'),
        template_index = '<div class="index"></div>',
        active_class = 'active';

      $wrapper.append(template_index);

      $itemFirst.imagesLoaded(function () {
        $itemFirst.addClass(active_class).fadeIn().siblings().fadeIn();

        $container.carouFredSel({
          items: {
            visible: 1,
            minimum: 2
          },
          scroll: {
            fx: 'crossfade',

            onBefore: function(data) {
              data.items.old.removeClass(active_class);
            },

            onAfter: function(data) {
              data.items.visible.addClass(active_class);
            }
          },
          auto: {
            timeoutDuration: 6000
          },
          pagination: $wrapper.find('.index'),
          responsive: true
        });

        main.nav.init();
      });
    },

    init: function() {
      var $wrapper = $(this.wrapper);

      if ($wrapper.length) {
        this.build();
      }
    }
  };
}(jQuery, this, this.document));
