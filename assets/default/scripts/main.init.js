;(function($, window, document, undefined) {
  'use strict';

  main.init = function() {
    var plugins = [
      'share',
      'form',
      'alert',
      'collapse',
      'togglebox',
      'modal',
      'dropdown',
      'scroll',
      'nav',
      'fieldExtend',
      'fieldCount',
      'fieldPicker',
      'lightbox',
      'billboard',
      'gallery'
    ];

    if (arguments.length) {
      plugins = arguments;
    }

    for (var i in plugins) {
      this[plugins[i]].init();
    }
  };

  main.init();
}(jQuery, this, this.document));
