;(function($, window, document, undefined) {
	'use strict';

	main.fieldPicker = {
		settings: {
			autoinit: true,
			main: '[data-field-picker]'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.builder();
			}
		},

		builder: function() {
			$(this.settings.main).each(function() {
				switch ($(this).data('fieldPicker')) {
					case 'date':
						$(this).datetimepicker({
							scrollInput: false,
							lang: 'pt',
							format: 'd/m/Y',
							timepicker: false,
							closeOnDateSelect: true
						});

						break;

					case 'time':
						$(this).datetimepicker({
							scrollInput: false,
							lang: 'pt',
							format: 'H:i',
							datepicker: false,
							step: 10
						});

						break;

					case 'datetime':
						$(this).datetimepicker({
							scrollInput: false,
							lang: 'pt',
							format: 'd/m/Y H:i',
							step: 10
						});

						break;

					default:
						break;
				}
			});
		}
	};
}(jQuery, this, this.document));

