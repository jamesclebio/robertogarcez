var main = main || {};

;(function($, window, document, undefined) {
  'use strict';

  main.formAjax = {
    settings: {
      autoinit: true,
      main: '[data-form-ajax]',
      errorTemplate: '<div class="alert alert-icon alert-danger"><p><strong>Ops! Algo deu errado...</strong></p><p>Por favor, verifique todos os campos e tente novamente.</p></div>',
      successTemplate: '<div class="alert alert-icon alert-success"><p><strong>Suas informações foram enviadas com sucesso!</strong></p><p>Entraremos em contato o mais breve possível.</p></div>'
    },

    init: function() {
      if ($(this.settings.main).length) {
        this.handler();
      }
    },

    handler: function() {
      var that = this,
        $main = $(this.settings.main);

      $main.on({
        submit: function(e) {
          that.post($(this));
          e.preventDefault();
        }
      });
    },

    message: function($refer, type) {
      var $message = $refer.parent().find('.alert');

      if ($message) {
        $message.remove();
      }

      switch (type) {
        case 'error':
          $refer.before(this.settings.errorTemplate);
          break;

        case 'success':
          $refer.before(this.settings.successTemplate);
          break;
      }

      basis.roll.to($refer.closest('.section-page'));
    },

    post: function($form) {
      var that = this,
        $section = $form.closest('.section-page-content'),
        url = $form.attr('action'),
        data = $form.serialize(),
        $button = $form.find('[type="submit"]'),
        buttonTextOrigin = $button.text(),
        buttonTextLoading = 'Enviando...';

      $.ajax({
        type: 'POST',
        url: url,
        data: data,

        beforeSend: function() {
          $button.attr('disabled', 'disabled').text(buttonTextLoading);
        },

        error: function() {
          that.message($form, 'error');
        },

        success: function(data) {
          that.message($section, 'success');
          $form.find(':input').val('');
        },

        complete: function() {
          $button.removeAttr('disabled').text(buttonTextOrigin);
        }
      });
    }
  };
}(jQuery, this, this.document));
