;(function($, window, document, undefined) {
  'use strict';

  main.scrollto = function(selector) {
    var $target = $(selector),
        $body = $('body, html');

    if (!$target.length) {
      $target = $body;
    }

    $body.animate({
      scrollTop: $target.offset().top - 99
    });
  };
}(jQuery, this, this.document));
