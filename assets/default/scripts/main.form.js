;(function($, window, document, undefined) {
  'use strict';

  main.form = {
    masks: function() {
      $('.mask-date').mask('99/99/9999');
      $('.mask-zipcode').mask('99999-999');
      $('.mask-cpf').mask('999.999.999-99');
      $('.mask-cnpj').mask('99.999.999/9999-99');

      $('.mask-phone').on({
        focusin: function() {
          $(this).mask('(99) 99999999?9');
        },

        focusout: function() {
          var phone = $(this).val().replace(/\D/g, '');

          if (phone.length > 10)
            $(this).mask('(99) 99999-999?9');
          else
            $(this).mask('(99) 9999-9999');
        }
      });
    },

    zipcode: function($input) {
      var $fields_update = $input.closest('form').find('[name="endereco"], [name="bairro"], [name="cidade"], [name="estado"]'),
        $alert = $input.next('.list-error'),
        zipcode = $input.val().replace(/\D/g, ''),
        class_loading = 'field-loading';

      if ($alert.length)
        $alert.remove();

      if (zipcode !== '') {
        $.ajax({
          url: 'http://cep.correiocontrol.com.br/' + zipcode + '.json',

          beforeSend: function() {
            $input.addClass('field-loading');
          },

          error: function() {
            $input.after('<ul class="list-error"><li>CEP Inválido! Tente novamente.</li></ul>');
            $fields_update.val('');
          },

          success: function(data) {
            $('[name="endereco"]').val(data.logradouro);
            $('[name="bairro"]').val(data.bairro);
            $('[name="cidade"]').val(data.localidade);
            $('[name="estado"]').val(data.uf);
            $('[name="numero"]').focus();
          },

          complete: function() {
            $input.removeClass(class_loading);
          }
        });
      } else
        $fields_update.val('');
    },

    bindings: function() {
      var that = this;

      // Zipcode Check
      $('.zipcode-check').on({
        change: function() {
          that.zipcode($(this));
        }
      });

      // Button Submit
      $('.button-submit').on({
        click: function(e) {
          if ($(this).attr('disabled') === undefined)
            $(this).closest('form').submit();

          e.preventDefault();
        }
      });
    },

    init: function() {
      var that = this;

      that.masks();
      that.bindings();
    }
  };
}(jQuery, this, this.document));
