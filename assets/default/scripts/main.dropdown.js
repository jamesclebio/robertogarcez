;(function($, window, document, undefined) {
  'use strict';

  main.dropdown = {
    wrapper: '.dropdown',

    bindings: function() {
      var that = this,
        $wrapper = $(that.wrapper);

      // Dropdown
      $wrapper.on({
        mouseenter: function() {
          $(this).clearQueue();
        },

        mouseleave: function() {
          $wrapper.prev().removeClass('active');
          $(this).fadeOut(0);
        }
      });

      // Trigger
      $wrapper.prev().on({
        click: function(e) {
          if ($(this).attr('href') === '#') {
            e.preventDefault();
          }
        },

        mouseenter: function() {
          var $dropdown = $(this).next();

          if ($dropdown.length && !$dropdown.is(':visible')) {
            $(this).addClass('active');
            $dropdown.clearQueue().fadeIn(0);
          }
        },

        mouseleave: function() {
          var $this = $(this),
            $dropdown = $(this).next();

          if ($dropdown.is(':visible')) {
            $dropdown.delay(10).fadeOut(0, function() {
              $this.removeClass('active');
            });
          }
        }
      });
    },

    init: function() {
      var that = this,
        $wrapper = $(that.wrapper);

      if ($wrapper.length) {
        that.bindings();
      }
    }
  };
}(jQuery, this, this.document));
