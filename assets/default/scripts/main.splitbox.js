;(function($, window, document, undefined) {
  'use strict';

  main.splitbox = {
    wrapper: '.splitbox',

    build: function() {
      var that = this,
        $wrapper = $(that.wrapper),
        active_class = 'active';

      $wrapper.find('section.' + active_class + ' .splitbox-content').show();
    },

    toggle: function($this) {
      var that = this,
        $splitbox = $this.closest(that.wrapper),
        $section = $this.closest('section'),
        active_class = 'active',
        speed = 100;

      if ($section.hasClass(active_class)) {
        $section.removeClass(active_class).find('.splitbox-content').slideUp(speed);
      } else {
        $splitbox.find('section.' + active_class).removeClass(active_class).find('.splitbox-content').slideUp(speed);
        $section.addClass(active_class).find('.splitbox-content').slideDown(speed);
      }
    },

    bindings: function() {
      var that = this,
        $wrapper = $(that.wrapper);

      $wrapper.find('header').on({
        click: function() {
          that.toggle($(this));
        }
      });
    },

    init: function() {
      var $wrapper = $(this.wrapper);

      if ($wrapper.length) {
        this.build();
        this.bindings();
      }
    }
  };
}(jQuery, this, this.document));
