;(function($, window, document, undefined) {
  'use strict';

  main.scroll = {
    bindings: function() {
      var that = this,
        $defaultHeader = $('.default-header'),
        $scrollTop = $('#scroll-top'),
        stickyClass = 'sticky-default-header',
        visibleClass = 'visible',
        scrollTrigger = ($('html').hasClass('layout-billboard')) ? 350 : 10;

      $(window).scroll(function() {

        // Billboard
        if ($(window).scrollTop() >= scrollTrigger) {
          $defaultHeader.addClass(stickyClass);
        } else {
          $defaultHeader.removeClass(stickyClass);
        }

        // Scroll top
        if ($(window).scrollTop() >= 200) {
          $scrollTop.addClass(visibleClass);
        } else {
          $scrollTop.removeClass(visibleClass);
        }
      });

      $scrollTop.on({
        click: function(e) {
          main.scrollto();
          e.preventDefault();
        }
      });
    },

    init: function() {
      this.bindings();
    }
  };
}(jQuery, this, this.document));
