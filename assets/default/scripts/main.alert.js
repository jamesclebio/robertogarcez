;(function($, window, document, undefined) {
	'use strict';

	main.alert = {
		wrapper: '.alert',
		template_close: '<a href="#" class="close icon-close-circled"></a>',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				if ($(this).data('alertClose')) {
					$(this).prepend(that.template_close);
				}
			});
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$(document).on({
				click: function(e) {
					$(this).closest(that.wrapper).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, that.wrapper + ' .close');
		},

		init: function() {
			this.build();
			this.bindings();
		}
	};
}(jQuery, this, this.document));
