;(function ($, window, document, undefined) {
  'use strict';

  main.share = {
    settings: {
      autoinit: true,
      main: '[data-share]'
    },

    init: function () {
      if ($(this.settings.main).length) {
        this.handler();
      }
    },

    handler: function () {
      var that = this;

      $(this.settings.main).on({
        click: function (e) {
          var service = $(this).data('share');

          if (service in that) {
            that[service]($(this));
          }

          e.preventDefault();
        }
      });
    },

    popup: function (url, name, width, height) {
      window.open(url, name, 'status=1,width='  + width  + ',height=' + height + ',top=' + ($(window).height() - height) / 2 + ',left=' + ($(window).width() - width) / 2);
    },

    base: function ($this) {
      return {
        service: $this.data('share'),
        url: $this.data('shareUrl') || window.location.href,
        text: $this.data('shareText') || document.title,
        via: $this.data('shareVia') || null
      };
    },

    twitter: function ($this) {
      var base = this.base($this),
        url = 'https://twitter.com/intent/tweet?url=' + base.url;

      if (base.text) {
        url += '&text=' + base.text;
      }

      if (base.via) {
        url += '&via=' + base.via;
      }

      this.popup(url, base.service, 575, 400);
    },

    facebook: function ($this) {
      var base = this.base($this),
        url = 'https://www.facebook.com/sharer/sharer.php?u=' + base.url;

      if (base.text) {
        url += '&text=' + base.text;
      }

      this.popup(url, base.service, 626, 436);
    },

    googleplus: function ($this) {
      var base = this.base($this),
        url = 'https://plus.google.com/share?u=' + base.url;

      this.popup(url, base.service, 600, 600);
    },

    pinterest: function ($this) {
      var base = this.base($this),
        url = 'http://pinterest.com/pin/create/button/?url=' + base.url;

      if (base.text) {
        url += '&description=' + base.text;
      }

      this.popup(url, base.service, 1020, 635);
    },

    linkedin: function ($this) {
      var base = this.base($this),
        url = 'http://www.linkedin.com/shareArticle?mini=true&url=' + base.url;

      if (base.text) {
        url += '&title=' + base.text;
      }

      this.popup(url, base.service, 520, 570);
    }
  };
}(jQuery, this, this.document));
