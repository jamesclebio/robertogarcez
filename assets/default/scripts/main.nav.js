;(function ($, window, document, undefined) {
  'use strict';

  main.nav = {
    wrapper: 'a[href*="#!/"]',

    build: function () {
      var that = this,
        $wrapper = $(that.wrapper),
        target = (/#!\//.test(window.location.hash)) ? window.location.hash.replace('!/', '') : false;

      if (target) {
        main.scrollto(target);
      }

      this.sectionContentHeight();
    },

    sectionContentHeight: function () {
      var $sectionContent = $('.section-content'),
          minHeight = $(window).height() - 100;

      if ($sectionContent) {
        $sectionContent.css('min-height', minHeight);
      }
    },

    bindings: function () {
      var that = this,
        $wrapper = $(that.wrapper);

      $wrapper.on({
        click: function (e) {
          var target = $(this).attr('href');

          if (/\/#/.test(target)) {
            target = target.replace(/.+#!\/?(\w+).*/, '#$1');
          } else {
            target = target.replace('!/', '');
          }

          main.scrollto(target);
        }
      });
    },

    init: function () {
      var that = this,
        $wrapper = $(that.wrapper);

      if ($wrapper.length) {
        that.build();
        that.bindings();
      }
    }
  };
}(jQuery, this, this.document));
