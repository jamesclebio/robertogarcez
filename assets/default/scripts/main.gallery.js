;(function ($, window, document, undefined) {
  'use strict';

  main.gallery = {
    settings: {
      autoinit: true,
      main: '.main-gallery',
      mainIndex: '.main-gallery-index',
      mainThumbs: '.main-gallery-thumbs',
      onClass: 'on'
    },

    init: function () {
      if ($(this.settings.main).length) {
        this.handler();
        this.builder();
      }
    },

    builder: function () {
      $(this.settings.mainIndex).find('.on a').trigger('click');
    },

    handler: function () {
      var that = this;

      // Index
      $(this.settings.mainIndex).find('a').on({
        click: function (e) {
          that.index($(this));
          e.preventDefault();
        }
      });
    },

    index: function ($this) {
      $this.closest('li').addClass(this.settings.onClass).siblings().removeClass(this.settings.onClass);
      this.thumbs($this.attr('href'), $this.closest(this.settings.main).find(this.settings.mainThumbs));
    },

    thumbs: function (url, $thumbs) {
      var that = this,
          $main = $(this.settings.main),
          emptyTemplate = '<div class="block-notice block-notice-empty"><h3><strong>Ops!</strong></h3><p>Ainda não temos galeria para o lugar selecionado...</p></div>',
          loadingTemplate = '<div class="block-loading">Carregando álbum...</div>',
          errorTemplate = '<div class="alert-short alert-short-danger"><h3><strong>Ops!</strong></h3><p>Algo deu errado... :(</p></div>';

      $.ajax({
        type: 'GET',
        url: url,

        beforeSend: function () {
          $thumbs.empty().append(loadingTemplate);
        },

        error: function () {
          $thumbs.empty().append(errorTemplate);
        },

        success: function (data) {
          $thumbs.empty().append(data);
          main.lightbox.init();
        }
    });
    }
  };
}(jQuery, this, this.document));
