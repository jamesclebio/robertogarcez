var main = main || {};

;(function($, window, document, undefined) {
  'use strict';

  main.lightbox = {
    settings: {
      autoinit: true,
      main: '.lightbox'
    },

    init: function () {
      if ($(this.settings.main).length) {
        this.builder();
      }
    },

    builder: function () {
      $(this.settings.main).colorbox();
    }
  };
}(jQuery, this, this.document));
